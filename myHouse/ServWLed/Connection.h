#include <ESP8266WiFi.h>
//port 16661

//Почему-то не рабоатет в классе
WiFiClient _client;
WiFiServer _server(16661);

class Connection
{
private:
	const char* _ssid;
	const char* _password;
  	long int lastTimeActivity;
	long int conectionTimeoutTime;
public:


	Connection(const char* ssid, const char* password, long int timeoutTime)
	{
		_ssid = ssid;
		_password = password;

		conectionTimeoutTime = timeoutTime;
		lastTimeActivity = millis();
	}


	//Инициализация соединения (обязательно)
	bool Initialize()
	{
		// Connect to WiFi network
		Serial.print("Connecting to ");
		Serial.println(_ssid);
		
		WiFi.begin(_ssid, _password);
		
		while (WiFi.status() != WL_CONNECTED) {
		  delay(500);
		  Serial.print(".");
		}
		Serial.println("");
		Serial.println("WiFi connected");
		
		// Start the server
		_server.begin();
		Serial.println("Server started");
	    // Print the IP address
		Serial.println(WiFi.localIP());

		return true;
	}

	int GetTimeoutTime(){
		return conectionTimeoutTime;
	}

	//Для перевода строки исп. \n
	void PrintToServer(String str)
	{
		Serial.println(str);
	}

	void PrintToClient(String str)
	{
		_client.println(str);
	}

	//Возвращает true, если клиент найден
	bool WaitForClient(bool isEnergySavingMode = false)
	{
		if (!isEnergySavingMode || millis() - lastTimeActivity > 1000)
		{
			lastTimeActivity = millis();
			_client = _server.available();
			if (_client){
				Serial.println("new client connected");
			}
			return _client;
		}
	}

	//Возвращает true, если последняя активность случилось позже, чем conectionTimeoutTime
	bool IsThereIsNoActivity()
	{
		if (millis() - lastTimeActivity > conectionTimeoutTime){
			lastTimeActivity = millis();
			return true;
		}
		else 
			return false;
	}

	//возвращает true, если есть новая активность. Также актуализирует lastTimeActivity
	bool IsNewBytes()
	{
		bool av = _client.available(); 
		if (av)
			lastTimeActivity = millis();
		return av;
	}

	String GetStringUntil(char ch)
	{
		return _client.readStringUntil(ch);
	}

	void Flush()
	{
		 _client.flush();
	}

	void StopClient()
	{
		_client.stop();
	}
};
