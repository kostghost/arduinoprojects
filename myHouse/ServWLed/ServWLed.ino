
#include "Requests.h" //Мой класс запросов
#include "Leds.h" 
#include "Connection.h"

#define HELP "available commands: help, status, exit, shine, putout, set ledscount [int], set delay [int], set mode {simple/surprize/rainbow}, set brightness (0 .. 1]"

#define IS_ENERGY_SAVING false

Connection con("Belka v4 Wi-Fi", "1q2w3e4r", 30000);
Leds leds;

void setup() {
    Serial.begin(115200);
    delay(10);
    
    con.Initialize();
    leds.InitializeLeds();
}

String getCurrentStatus(){
  String s = "Leds is now ";
  s += (leds.isNowShines)?"ON":"OFF";
  s += "\r\ncurrent mode is: (0 - SIMPLE, 1 - SURPRIZE, 2 - RAINBOW)";
  s += leds.currentModeOfShine;
  s += "\r\nDelay now is: ";
  s += leds.GetCurrentDelay();
  s += "\r\nBrightness:";
  s += leds.GetBrightness();
  s += "\r\nNum of leds: " ;
  s += leds.GetCurrentNumOfLeds();
  s += "\r\nConnection Timeout time: ";
  s += con.GetTimeoutTime();
  s += "\r\n";

  return s;
}

//Работает вне зависимости от состояния соединения
void IndependentLoop()
{
    //СВЕТОДИОНДАЯ РАБОТА
    if (leds.isNowShines)
      leds.LoopLeds();
}

//Основное поведение при обработке запроса тут.
bool MakeBehaviour(Request request)
{
  bool isExit = false;
  
    if (request.req == "status"){
        con.PrintToClient("server> " + getCurrentStatus());
    }
    else if (request.req == "exit"){
        isExit = true;
        con.StopClient();
    }
    else if (request.req == "help"){
        con.PrintToClient(HELP);
    }
    else if (request.req == "shine"){
        con.PrintToClient("Leds turned on");
        leds.isNowShines = true;
    }
    else if (request.req == "set mode surprize"){
        con.PrintToClient("surprize mfck");
        leds.currentModeOfShine = mods.SURPRIZE;
    }
    else if (request.req == "putout"){
        leds.TurnOffLeds();
        leds.isNowShines = false;
    }
    else if (request.req == "set"){
        if (request.countOfArguments == 2 && request.arguments[0] == "ledscount"){
            leds.ChangeLedsCount(request.arguments[1].toInt());
        }
        else if (request.countOfArguments == 2 && request.arguments[0] == "delay"){
            leds.ChangeDelay(request.arguments[1].toInt());
        }
        else if (request.countOfArguments == 2 && request.arguments[0] == "brightness"){
            leds.SetBrightness(request.arguments[1].toFloat());
        }
        else if (request.countOfArguments == 2 && request.arguments[0] == "mode"){
            if (request.arguments[1] == "surprize"){
              leds.currentModeOfShine = mods.SURPRIZE;
            }
            else if (request.arguments[1] == "simple"){
              leds.currentModeOfShine = mods.SIMPLE;
            }
            else if (request.arguments[1] == "rainbow"){
              leds.currentModeOfShine = mods.RAINBOW;
            }
            else {
              con.PrintToClient("bad mode, type 'help' for hint");
            }
            
        }
        
        else {
            con.PrintToClient("bad syntax of 'set'");
        }

    }
    else {
        con.PrintToServer("server> invalid request");
        con.PrintToClient("server> invalid request");
        //client.stop();
    }

    return isExit;
}

void loop() {
    IndependentLoop();

    if (con.WaitForClient(IS_ENERGY_SAVING))
    {
        con.PrintToClient(HELP);
        con.Flush();
        
        bool isExit = false;
        //bool isNewBytes = false;
        while (!isExit) //Цикл, когда мы уже подключились к юзеру
        {
            IndependentLoop();
            //Проверяем, есть ли новые данные от клиента
            if (con.IsNewBytes())  //Выполняем только когда юзер кинул новых данных
            {
                Request request;
                String req = con.GetStringUntil('\r'); //Читаем первую строчку запроса
                request.ReadRequest(req); //Парсим наш реквест

                Serial.println("client> " + request.GetDecoratedString());
                con.Flush();

                isExit = MakeBehaviour(request);
         
                con.Flush();
            }

            //останавливает клиент, если давно нет активности
            if(con.IsThereIsNoActivity()){
                isExit = true;
                con.PrintToClient("Client disonnected. Timeout.");
                con.PrintToServer("Client disonnected. Timeout.");
                con.StopClient();
            }
        }

        Serial.println("Client disonnected");
    } 
}

