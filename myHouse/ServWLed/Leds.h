#include <Adafruit_NeoPixel.h>
#define LED_PIN D6
#define LED_COUNT 46
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);  //Вся эта шняга работает только вне класса. Иначе - непредвиденные ошибки.

struct ShineMods
{
  enum {SIMPLE, SURPRIZE, RAINBOW};
} mods;

class Leds
{
private:
	unsigned int workingLeds; 
	int delayLed;
	unsigned long lastTickTimeLoopLeds;
	int currentColorSum;
	double brightnessCoef; //TODO Стоит поменять вызов функции Color, чтобы не домножать каждый коэф. отдельно


  //Возвращает цвет пикселя для Радуги
  uint32_t Wheel(byte WheelPos) {
    WheelPos = 255 - WheelPos;
    if(WheelPos < 85) {
      return pixels.Color(int((255 - WheelPos * 3) * brightnessCoef), 0, int( (WheelPos * 3) * brightnessCoef));
    }
    if(WheelPos < 170) {
      WheelPos -= 85;
      return pixels.Color(0, int(WheelPos * 3 * brightnessCoef), int( (255 - WheelPos * 3) * brightnessCoef ));
    }
    WheelPos -= 170;
    return pixels.Color(int( (WheelPos * 3) * brightnessCoef ), int( (255 - WheelPos * 3) * brightnessCoef) , 0);
  }
 
public:
	bool isNowShines;
  	int currentModeOfShine;


  void InitializeLeds()  {
      pixels.begin();
      pixels.show(); // Устанавливаем все светодиоды в состояние "Выключено"
      Serial.println("leds initialized succesfully");
  }

	Leds(){
    currentModeOfShine = mods.RAINBOW;
		workingLeds = LED_COUNT;
		delayLed = 30;
		isNowShines = true;
		lastTickTimeLoopLeds = 0;
		currentColorSum = 0;
		brightnessCoef = 0.99;
	}

	void ChangeLedsCount(int count)
	{
	    if (count < LED_COUNT)
	    {
	        workingLeds = count;
	         for (int i=count; i<LED_COUNT; i++ )
	        {
	            pixels.setPixelColor(i, pixels.Color(0,0,0));
	        }
	        pixels.show();
	    }
	    //client.print("now is ");
	    //client.print(workingLeds);
	    //client.println(" leds");
	}

  void SetBrightness(double coefficent)
  {
    if (coefficent <= 1 && coefficent > 0)
    {
      brightnessCoef = coefficent;
    }
  }

  double GetBrightness()
  {
    return brightnessCoef;
  }

	void ChangeDelay(int count){
	    delayLed = count;
	    //client.print("delay is now ");
	    //client.println(delayLed);
	}

	int GetCurrentDelay(){
		return delayLed;
	}

	int GetCurrentNumOfLeds(){
		return workingLeds;
	}

 //Разделить на несколько функций
	void LoopLeds(){
	    
	    if(millis() - lastTickTimeLoopLeds > delayLed)
	    {
        if (currentModeOfShine == mods.SIMPLE) ///ГОВНО ТУТ TODO
        {
  	        for (int i=0; i<workingLeds; i++ )
  	        {
  	            pixels.setPixelColor(i, pixels.Color( int(((i+currentColorSum)*3)%255 * brightnessCoef),
  	            									  int(((i+currentColorSum)*7)%255 * brightnessCoef),
  	            									  int(((i+currentColorSum)*11)%255 * brightnessCoef)));
  	            lastTickTimeLoopLeds = millis();
  	        }
           currentColorSum++;
        }
        else if (currentModeOfShine == mods.SURPRIZE)
        {
          for (int i=0; i<workingLeds; i++ )
            {
                pixels.setPixelColor(i, pixels.Color(int(200 * brightnessCoef),
                									 int(150 * brightnessCoef),
                									 int(50  * brightnessCoef)) );
                currentColorSum++;
                lastTickTimeLoopLeds = millis();
            }
        }
        else if (currentModeOfShine == mods.RAINBOW)
        {
          for (int i = 0; i < workingLeds; i++)
          {
            pixels.setPixelColor(i, Wheel( (currentColorSum + i) & 255) );
            lastTickTimeLoopLeds = millis();
          }
          currentColorSum++;
        }
        
	        pixels.show();
	    }
	}

	void TurnOffLeds(){
		for (int i=0; i<LED_COUNT; i++ )
		{
		    pixels.setPixelColor(i, pixels.Color(0,0,0));
		    currentColorSum++;
	 	}
		pixels.show();
		//client.println("server> Leds turned off");
	}

};
