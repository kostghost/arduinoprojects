class Request
{
private:
    static const int MAX_COUNT_OF_ARGUMENTS = 10;
  
public:
    int countOfArguments;
    String req;
    String arguments[MAX_COUNT_OF_ARGUMENTS];

    Request(){
        countOfArguments = 0;
        req = "";
        for (int i = 0; i < MAX_COUNT_OF_ARGUMENTS; i++)
        {
            arguments[i] = "";
        }
    }

    void ReadRequest(String str){
        countOfArguments = 0;
        str.toLowerCase(); //бесит этот ардуиновский стиль
        str.trim();

        int preSpacePos = 0;
        int spacePos = str.indexOf(' ', preSpacePos); //ищем пробел

        //читаем сам запрос
        if (spacePos == -1)
            req = str;
        else
            req = str.substring(0, spacePos);

        //Читаем аргументы
        while (spacePos != -1)
        {
            preSpacePos = spacePos;
            spacePos = str.indexOf(' ', preSpacePos + 1);

            if (spacePos == -1) //Если достигли конца
                arguments[countOfArguments] = str.substring(preSpacePos + 1, str.length());
            else
                arguments[countOfArguments] = str.substring(preSpacePos + 1, spacePos);

            countOfArguments++;
        }
    }

    String GetDecoratedString()
    {
        String result = req;

        if (countOfArguments > 0){
            result = result + " args(" + countOfArguments + ")[ ";
            for (int i = 0; i < countOfArguments; i++)
            {
                result = result + arguments[i] + " ";
            }
            result = result + "]";
        }
        return result;
    }
};